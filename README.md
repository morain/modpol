# ModPol

Programs and pdf's to understand and use modular polynomials of various types.
Emphasis now is for genus 1.

Tested with Magma V2.23-6

## Typical use:

load "include.mag";

UX[5]:=ModPolCompute(5, ["U"], <"series", 0>)[1][2];

ModPolCheckPolynom(5, UX[5], "U", "E46D", "splitting", 10^5, 1, 10);

ModPolCheckPolynom(5, UX[5], "U", "E46D", "isogeny", 10^5, 1, 10);

DEP:=[];

DEP[7]:=ModPolCompute(7, ["eta"], <"series", 0>);
